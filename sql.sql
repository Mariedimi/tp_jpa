
DROP TABLE IF EXISTS professeurs_instruments;
CREATE TABLE professeurs_instruments (
  id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nom varchar(30),
  instrument varchar(30)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS  eleves_instruments;
CREATE TABLE eleves_instruments (
  id int  NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nom varchar(30),
  instruments varchar(30)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  