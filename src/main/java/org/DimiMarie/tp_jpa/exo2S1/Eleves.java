package org.DimiMarie.tp_jpa.exo2S1;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;

import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(
   name="eleves",
   uniqueConstraints={
       @UniqueConstraint(name="nom", columnNames={"nom"})
   }
)
public class Eleves {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "nom", length = 40)
	private String name;
	

	@Column(name="age")
	private int age;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getAge() {
		return age;
	}


	public void setAge(int age) {
		this.age = age;
	}
    

	@Override
	public String toString() {
		return "Eleves [id=" + id + ", name=" + name + ", age=" + age + "]";
	}
	
	@ManyToMany
	private Set<Instruments> instruments= new HashSet<Instruments>();
	
	@ManyToMany(mappedBy="eleves")

	private Set<Professeurs> professeurs= new HashSet<Professeurs>();
	
	
	
}
