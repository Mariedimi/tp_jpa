package org.DimiMarie.tp_jpa.exo2S1;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name = "instruments",uniqueConstraints = { @UniqueConstraint(name = "nom", columnNames = { "nom" }) })
public class Instruments {
	
	public Instruments() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "nom", length = 40)
	private String name;

	private int prix;
	private int prix_cours;

	
	private char location;

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public int getPrixCours() {
		return prix_cours;
	}

	public void setPrixCours(int prixCours) {
		this.prix_cours = prixCours;
	}



	public char getLocation() {
		return location;
	}

	public void setLocation(char location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "Instruments [id=" + id + ", name=" + name + ", prix=" + prix + ", prix_cours=" + prix_cours
				+ ", location=" + location + "]";
	}

	@ManyToMany(mappedBy="instruments")
	private Set<Eleves> eleves = new HashSet<Eleves>();
	
	@OneToOne
	private Professeurs professeur;
	
}
