package org.DimiMarie.tp_jpa.exo2S1;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class Main {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("jpa-test");
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		System.out.println("base de données connectée");

		/*** Question 1 ***/
		System.out.println("*** Question 1 ***");
		Query queries = entityManager.createQuery("SELECT e FROM Eleves e");
		 List<Eleves> eleves = queries.getResultList();
		 for (Eleves e : eleves) {
		 System.out.println("Eleve = " + e);
		 }

		/*** Question 2a ***/
		 System.out.println("*** Question 2a ***");
		Instruments instrument = new Instruments();
		instrument.setName("Saxophone");
		instrument.setPrix(1000);
		instrument.setPrixCours(15);
		instrument.setLocation('0');
		
//taille de la table pour trouver l'identifiant
		queries = entityManager.createQuery("SELECT i FROM Instruments i");
		int taille = queries.getResultList().size();
	//	System.out.println(taille);
		 List<Instruments> profs =(List<Instruments>)queries.getResultList();
		 for (Instruments e : profs) {
		 System.out.println(e);
		 }
//		instrument.setId(taille+1);
//		
//		
//		entityManager.getTransaction().begin();
//		entityManager.merge(instrument);
//		entityManager.getTransaction().commit();
//
//		queries = entityManager.createQuery("SELECT i FROM Instruments i where i.name='Saxophone'");
//		instrument = (Instruments) queries.getSingleResult();
//		System.out.println("instrument = " + instrument);
//
//		/*** Question 2b ***/
//		 System.out.println("*** Question 2b ***");
//
//		 int result=entityManager.createQuery("UPDATE Professeurs SETname='Victor',age=15,instrument="+instrument.getId()).executeUpdate();
//		 queries = entityManager.createQuery("SELECT p FROM Professeurs p");
//		 List<Professeurs> profs1 = queries.getResultList();
//		 for (Professeurs e : profs1) {
//		 System.out.println("prof = " + e);
//		 }

	}

}
