package org.DimiMarie.tp_jpa.exo2S1;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity(name = "Professeurs")
@Table(name = "professeurs", uniqueConstraints = { @UniqueConstraint(name = "nom", columnNames = { "nom" }) })
public class Professeurs {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "nom", length = 40)
	private String name;

	@Column(name = "age")
	private int age;

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@OneToOne(mappedBy = "professeur")
	private Instruments discipline;

	@ManyToMany
	private Set<Eleves> eleves= new HashSet<Eleves>();

	@Override
	public String toString() {
		return "Professeurs [id=" + id + ", name=" + name + ", age=" + age + "]";
	}
}
