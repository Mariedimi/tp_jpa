package org.paumard.tpjpa;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.paumard.tpjpa.model.User;

public class Main {

	public static void main(String[] args) {

		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("jpa-test");

		EntityManager entityManager = entityManagerFactory.createEntityManager();

		 Calendar calendar = new GregorianCalendar(1995, 5, 20);
		 Date date1 = calendar.getTime();
		 calendar = new GregorianCalendar(1999, 5, 20);
		 Date date2 = calendar.getTime();
		 User user1 = new User("Kylian", "james", date1);
		 User user2 = new User("Paul","Brown", date2);
		
		 System.out.println("User 1 = " + user1);
		 System.out.println("User 2 = " + user2);
		 entityManager.getTransaction().begin();
		 entityManager.persist(user1);
		 entityManager.persist(user2);
		 entityManager.getTransaction().commit();
		Query q1 = entityManager.createQuery("SELECT u FROM User u");
		List<User> users = q1.getResultList();
		for (User u : users) {
			System.out.println("User  = " + u);}
		}

	}

