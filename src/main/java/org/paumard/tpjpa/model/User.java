package org.paumard.tpjpa.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Persistence;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.persistence.criteria.CriteriaQuery;

import org.paumard.tpjpa.util.Civility;

@Entity
@Table(name = "users", uniqueConstraints = {
		@UniqueConstraint(name = "nom_prenom", columnNames = { "first_name", "last_name" }) })
public class User implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "first_name", length = 40)
	private String firstName;
	
	@Column(name = "last_name", length = 40)
	private String lastName;

	@Column(name="date_de_naissance")
	@Temporal(TemporalType.DATE)
	private Date dateOfBirth;
	// @Enumerated(EnumType.STRING)
	// private Civility civility;

	
	public User() {
	}

	public User(String firstname, String lastname, Date dateOfBirth) {
		this.firstName = firstname;
		this.lastName = lastname;
		this.dateOfBirth = dateOfBirth;
		// this.civility = civility;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	// public String getName() {
	// return name;
	// }
	//
	// public void setName(String name) {
	// this.name = name;
	// }

//	public int getAge() {
//		return age;
//	}
//
//	public void setAge(int age) {
//		this.age = age;
//	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	// public Civility getCivility() {
	// return civility;
	// }
	//
	// public void setCivility(Civility civility) {
	// this.civility = civility;
	// }

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", dateOfBirth=" + dateOfBirth
				+ "]";
	}

}
