package org.paumard.tpjpa.model;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class UserFactory {
	EntityManager entityManager;
	
	public UserFactory()
	{
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("jpa-test");
	 entityManager = entityManagerFactory.createEntityManager();
	}
	
	public User createUser(String firstName, String lastName, Date dateOfBirth) {
		User user = new User(firstName, lastName, dateOfBirth);
		entityManager.getTransaction().begin();
		entityManager.persist(user);
		entityManager.getTransaction().commit();
		return user;
	}

	User find(long id) {
		entityManager.getTransaction().commit();
		User user = entityManager.find(User.class, id);
		entityManager.getTransaction().commit();
		return user;
	}
	
	void delete(long id) {
		entityManager.getTransaction().commit();
		entityManager.remove(find(id));
		entityManager.getTransaction().commit();
	}

}
